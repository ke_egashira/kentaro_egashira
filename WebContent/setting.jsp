<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー編集</title>
	</head>
	<body>
		<div class="main-contents">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				 <c:remove var="errorMessages" scope="session" />
			</c:if>

			<form action="setting" method="post"><br />

				<a href="./manegement">ユーザー管理</a>  <br />

					<input type ="hidden" name="userId" value="${user.id}" />

					<label for="account">アカウント</label>
					<input name="account" id="account" value="${user.account}" /> <br />

					<label for="password">パスワード</label>
					<input name="password" type="password" id="password" /> <br />

					<label for="password">確認用パスワード</label>
					<input name="passwordConfirm" type="password" id="passwordConfirm" /> <br />

					<label for="name">名前</label>
					<input name="name" id="name" value="${user.name}" /> <br />

					<c:if test="${ loginUser.id != user.id }" >
						<label for="branchId">支社</label>
						<select name="branchId">
								<c:forEach items="${branches}" var="branch">
									<option ${ user.branchId == branch.id? 'selected':' '  } value="${branch.id}" >${branch.name} </option>
								</c:forEach>
						</select> <br />

						<label for="departmentId">部署</label>
						<select name="departmentId">
								<c:forEach items="${departments}" var="department">
									<option ${ user.departmentId == department.id? 'selected':' ' } value="${department.id}">${department.name} </option>
								</c:forEach>
						</select> <br />
					</c:if>

					<c:if test="${ loginUser.id == user.id }" >
						<label for="branchId">支社</label>
						<select name="branchId">
								<c:forEach items="${branches}" var="branch">
									<option ${ user.branchId == branch.id? 'selected':'disabled'  } value="${branch.id}" >${branch.name} </option>
								</c:forEach>
						</select> <br />

						<label for="departmentId">部署</label>
						<select name="departmentId">
								<c:forEach items="${departments}" var="department">
									<option ${ user.departmentId == department.id? 'selected':'disabled' } value="${department.id}">${department.name} </option>
								</c:forEach>
						</select> <br />
					</c:if>
					<input type="submit" value="変更" /> <br />
				</form>
			<div class="copyright">
			<p>
				<small>Copyright(c)Kentaro Egashira</small>
			</p>
		</div>
		</div>
	</body>
</html>