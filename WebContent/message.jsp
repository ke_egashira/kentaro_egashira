<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿</title>
		<link type="text/css" href="/css/styles.css" rel="stylesheet">
	</head>
	<body>
		<div class="header">
		<a href="./">ホーム</a>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						 <li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="form-area">
			<form action="message" method="post"><br />
				<label for="title">タイトル</label>
				<input name="title" id="title" value="${title}"/> <br />

				<label for="category">カテゴリ</label>
				<input name="category" id="category" value="${category}"/> <br />

				<label for="text">投稿内容</label>
				<textarea name="text" cols="20" rows="10" wrap="hard"  class="message-box" >${text}</textarea> <br />
				<input type="submit" value="投稿" >
			</form>
			<div class="copyright">
			<p>
				<small>Copyright(c)Kentaro Egashira</small>
			</p>
		</div>
		</div>
	</body>
</html>