<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理</title>
	</head>
	<body>

	<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				 <c:remove var="errorMessages" scope="session" />
			</c:if>

		<div class="header">
			<a href="./">HOME</a>
			<a href="signup">ユーザー新規登録</a>
		</div>
		<div class="users">
			<c:forEach items="${usersManegement}" var="user">
				<div class="account">アカウント名：<c:out value="${user.account}" /></div>
				<div class="user">ユーザー名：<c:out value="${user.name}" /></div>
				<div class="branch">支店名：<c:out value="${user.branchName}" /></div>
				<div class="department">部署名：<c:out value="${user.departmentName}" /></div>

				<c:set var="isStopped">0</c:set>
				<c:if test="${ user.isStopped == isStopped }">
					<div class="name">状態：<c:out value="ACTIVE" /></div>
				</c:if>

				<c:if test="${ user.isStopped != isStopped }">
					<div class="name">状態：<c:out value="NON ACTIVE" /></div>
				</c:if>

				<form action="setting" method="get"><br />
					<input type ="hidden" name="userId" value="${user.id}" />
					<input type="submit" value="編集">
				</form>

				<c:if test="${ loginUser.id != user.id}">
					<form action="stop" method="post"><br />
						<c:if test="${ user.isStopped != isStopped }">
							<input type ="hidden" name="isStop" value="0" />
							<input type ="hidden" name="userId" value="${user.id }" />
							<input type="submit" value="復活" onclick='return confirm("このユーザーを復活させます");'>
						</c:if>

						<c:if test="${ user.isStopped == isStopped }">
							<input type ="hidden" name="isStop" value="1" />
							<input type ="hidden" name="userId" value="${user.id }" />
							<input type="submit" value="停止" onclick='return confirm("このユーザーを停止します");'>
						</c:if>
					</form>
				</c:if>
			</c:forEach>
		</div>
	</body>
</html>