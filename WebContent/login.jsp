<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログイン</title>
		<link type="text/css" href="./css/style.css" rel="stylesheet">
	</head>
	<body>
		<div class="main-contents">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<form action="login" method="post"><br />
				<label for="accountl">アカウント</label>
				<input name="account" id="account"  value="${account}"/> <br />

				<label for="password">パスワード</label>
				<input name="password" type="password" id="password"/> <br />

				<input type="submit" value="ログイン" /> <br />
			</form>
			<div class="copyright">

				<small>Copyright(c)Kentaro Egashira</small>

		</div>
		</div>
	</body>
</html>