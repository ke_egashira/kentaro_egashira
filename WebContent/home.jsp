<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link type="text/css" href="./css/home.css" rel="stylesheet">
		<title>HOME</title>
	</head>

	<body>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./message">新規投稿</a>
				<c:if test="${loginUser.departmentId == 1}">
					<a href="./manegement">ユーザー管理</a>
				</c:if>
				<a href="logout">ログアウト</a>
			</c:if>

		<div class="sarch-message">
			<c:if test="${ not empty loginUser }">
				<form action="./" method="get"><br />
					<label for="lengt">LENGTH</label><br />
						<input name="startDate" type="date" value="${startDate}" placeholder="Start"></input>
						<input name="endDate" type="date" value="${endDate}" placeholder="End"> <br />
						<label for="category">CATEGORY</label><br />
						<input name="category" id="category" value="${category}"/>
					<input type="submit" value="絞り込み" class="button">
				</form>
			</c:if>
		</div>
		</div>
	<div class="main-content">
		<c:if test="${ not empty loginUser }">
			<div class="messages">

				<c:forEach items="${messages}" var="message">
				<div class="left-line">
					<div class="message">
						<div class="name"><h3><c:out value="${message.name}" /></h3></div>
						<div class="title"><c:out value="${message.title}" /></div>
						<div class="category"><c:out value="${message.category}" /></div>
						<div class="text"><c:out value="${message.text}" /></div>
						<div class="date"><small><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></small></div>

						<c:if test="${ loginUser.id == message.userId }">
							<form action="deleteMessage" method="post"><br />
								<input type ="hidden" name="deleteId" value="${message.id }" />
								<input type="submit" value="削除" class="button" onclick='return confirm("メッセージを削除します");'>
							</form>
						</c:if>
					</div>

					<div class="comments">
						<c:forEach items="${comments}" var="comment">
							<c:if test="${ message.id == comment.messageId }">
								<div class="name"><c:out value="${comment.name}" /></div>
								<div class="text"><c:out value="${comment.text}" /></div>
								<div class="date"><small><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></small></div>
								<c:if test="${ loginUser.id == comment.userId }">
									<form action="deleteComment" method="post"><br />
										<input type ="hidden" name="deleteId" value="${comment.id }" />
										<input type="submit" value="コメント削除" class="button" onclick='return confirm("コメントを削除します");'>
									</form>
								</c:if>
							</c:if>
						</c:forEach>
					</div>
					</div>

					<div class="comment-form">
						<form action="comment" method="post"><br />
							<div class="message-id">
								<input type ="hidden" name="messageId" value="${message.id }" />
							</div>
							<label for="text">コメント</label> <br />
							<textarea name="text" cols="30" rows="5"  class="comment-box"></textarea>
							<br />
							<input type="submit" class="button" value="投稿">
						</form>
					</div>
				</c:forEach>
			</div>
		</c:if>
		</div>
		<div class="copyright">
			<p>
				<small>Copyright(c)Kentaro Egashira</small>
			</p>
		</div>
</body>
</html>