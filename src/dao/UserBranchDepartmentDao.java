package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;


public class UserBranchDepartmentDao {
    public List<UserBranchDepartment> select(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("    users.id, ");
            sql.append("    users.account, ");
            sql.append("    users.name, ");
            sql.append("    branches.name AS branch_name, ");
            sql.append("    departments.name AS department_name, ");
            sql.append("    users.is_stopped");
            sql.append(" FROM users ");
            sql.append(" INNER JOIN branches ");
            sql.append(" ON users.branch_id = branches.id");
            sql.append(" INNER JOIN departments ");
            sql.append(" ON users.department_id = departments.id");
            sql.append(" ORDER BY is_stopped DESC;" );

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();

            List<UserBranchDepartment> usersManagement = toUserBranchDepartment(rs);
            return usersManagement;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserBranchDepartment> toUserBranchDepartment(ResultSet rs) throws SQLException {

        List<UserBranchDepartment> usersManegement = new ArrayList<UserBranchDepartment>();
        try {
            while (rs.next()) {
            	UserBranchDepartment userManegement = new UserBranchDepartment();
            	userManegement.setId(rs.getInt("id"));
            	userManegement.setAccount(rs.getString("account"));
            	userManegement.setName(rs.getString("name"));
            	userManegement.setBranchName(rs.getString("branch_name"));
            	userManegement.setDepartmentName(rs.getString("department_name"));
            	userManegement.setIsStopped(rs.getInt("is_stopped"));

            	usersManegement.add(userManegement);
            }
            return usersManegement;
        } finally {
            close(rs);
        }
    }
}