package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("    title, ");
            sql.append("    category, ");
            sql.append("    text, ");
            sql.append("    user_id, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");
            sql.append("    ?, ");
            sql.append("    ?, ");
            sql.append("    ?, ");
            sql.append("    CURRENT_TIMESTAMP, ");
            sql.append("    CURRENT_TIMESTAMP ");
            sql.append(");");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, message.getTitle());
            ps.setString(2, message.getCategory());
            ps.setString(3, message.getText());
            ps.setInt(4, message.getUserId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void delete(Connection connection, Message  message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM messages");
            sql.append("    WHERE");
            sql.append("    id=");
            sql.append("    ? ");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}