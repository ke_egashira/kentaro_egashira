package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;


public class UserMessageDao {
    public List<UserMessage> getMessage(Connection connection, String setStartDate, String setEndDate, String setCategory) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("    messages.id, ");
            sql.append("    messages.title, ");
            sql.append("    messages.category, ");
            sql.append("    messages.text, ");
            sql.append("    messages.user_id, ");
            sql.append("    users.name, ");
            sql.append("    users.account, ");
            sql.append("    messages.created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE messages.created_date " );
            sql.append("BETWEEN ? AND ? AND" );
            sql.append(" messages.category like ?  " );
            sql.append(" ORDER BY messages.created_date DESC; " );

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, setStartDate);
            ps.setString(2, setEndDate);
            ps.setString(3, setCategory);

            ResultSet rs = ps.executeQuery();

            List<UserMessage> messages = toUserMessages(rs);
            return messages;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

        List<UserMessage> messages = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                UserMessage message = new UserMessage();
                message.setId(rs.getInt("id"));
                message.setTitle(rs.getString("title"));
                message.setCategory(rs.getString("category"));
                message.setText(rs.getString("text"));
                message.setUserId(rs.getInt("user_id"));
                message.setName(rs.getString("name"));
                message.setAccount(rs.getString("account"));
                message.setCreatedDate(rs.getTimestamp("created_date"));

                messages.add(message);
            }
            return messages;
        } finally {
            close(rs);
        }
    }
}