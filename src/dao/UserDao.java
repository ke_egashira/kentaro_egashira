package dao;
import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
//import jp.alhinc.egashira_kentaro.exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("    account, ");
            sql.append("    password, ");
            sql.append("    name, ");
            sql.append("    branch_id, ");
            sql.append("    department_id, ");
            sql.append("    is_stopped, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");
            sql.append("    ?, ");
            sql.append("    ?, ");
            sql.append("    ?, ");
            sql.append("    ?, ");
            sql.append("    DEFAULT, ");
            sql.append("    CURRENT_TIMESTAMP, ");
            sql.append("    CURRENT_TIMESTAMP ");
            sql.append(");");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getDepartmentId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User select(Connection connection, String account, String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ? AND password = ?;";

            ps = connection.prepareStatement(sql);

            ps.setString(1, account);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> users = toUsers(rs);
            if (users.isEmpty()) {
                return null;
            } else if (2 <= users.size()) {
                throw new IllegalStateException("ユーザーが重複しています");
            } else {
                return users.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUsers(ResultSet rs) throws SQLException {

        List<User> users = new ArrayList<User>();
        try {
            while (rs.next()) {
            	 User user = new User();
            	 user.setId(rs.getInt("id"));
            	 user.setAccount(rs.getString("account"));
                 user.setPassword(rs.getString("password"));
                 user.setName(rs.getString("name"));
                 user.setBranchId(rs.getInt("branch_id"));
                 user.setDepartmentId(rs.getInt("department_id"));
                 user.setCreatedDate(rs.getTimestamp("created_date"));
                 user.setUpdatedDate(rs.getTimestamp("updated_date"));
                 user.setIsStopped(rs.getInt("is_stopped"));

                 users.add(user);
            }
            return users;
        } finally {
            close(rs);
        }
    }

    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users  SET ");
            sql.append("    account= ");
            sql.append("    ?, ");
            sql.append("    name= ");
            sql.append("    ?, ");
            sql.append("    password= ");
            sql.append("    ?, ");
            sql.append("    branch_id= ");
            sql.append("    ?, ");
            sql.append("    department_id= ");
            sql.append("    ?, ");
            sql.append("    updated_date=");
            sql.append("    CURRENT_TIMESTAMP ");
            sql.append("    WHERE id= ");
            sql.append("    ?; ");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getDepartmentId());
            ps.setInt(6, user.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void getState(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users  SET ");
            sql.append("    is_stopped=?,  ");
            sql.append("    updated_date=");
            sql.append("    CURRENT_TIMESTAMP ");
            sql.append("    WHERE id=?; ");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, user.getIsStopped());
            ps.setInt(2, user.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User selectUser(Connection connection, String account) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ?;";

            ps = connection.prepareStatement(sql);

            ps.setString(1, account);

            ResultSet rs = ps.executeQuery();
            List<User> users = toUpdateUsers(rs);
            if (users.isEmpty()) {
                return null;
            } else {
                return users.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUpdateUsers(ResultSet rs) throws SQLException {

        List<User> users = new ArrayList<User>();
        try {
            while (rs.next()) {
            	 User user = new User();
            	 user.setId(rs.getInt("id"));
            	 user.setAccount(rs.getString("account"));
                 user.setPassword(rs.getString("password"));
                 user.setName(rs.getString("name"));
                 user.setBranchId(rs.getInt("branch_id"));
                 user.setDepartmentId(rs.getInt("department_id"));
                 user.setCreatedDate(rs.getTimestamp("created_date"));
                 user.setUpdatedDate(rs.getTimestamp("updated_date"));
                 user.setIsStopped(rs.getInt("is_stopped"));

                 users.add(user);
            }
            return users;
        } finally {
            close(rs);
        }
    }

    public User selectUser(Connection connection, int userId) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?;";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, userId);

            ResultSet rs = ps.executeQuery();
            List<User> user = toUser(rs);
            if (user.isEmpty()) {
                return null;
            } else {
                return user.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUser(ResultSet rs) throws SQLException {

        List<User> users = new ArrayList<User>();
        try {
            while (rs.next()) {
            	 User user = new User();
            	 user.setId(rs.getInt("id"));
            	 user.setAccount(rs.getString("account"));
                 user.setPassword(rs.getString("password"));
                 user.setName(rs.getString("name"));
                 user.setBranchId(rs.getInt("branch_id"));
                 user.setDepartmentId(rs.getInt("department_id"));
                 user.setCreatedDate(rs.getTimestamp("created_date"));
                 user.setUpdatedDate(rs.getTimestamp("updated_date"));
                 user.setIsStopped(rs.getInt("is_stopped"));

                 users.add(user);
            }
            return users;
        } finally {
            close(rs);
        }
    }
}