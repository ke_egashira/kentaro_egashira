package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;


@WebFilter(urlPatterns = {"/signup", "/setting", "/manegement"})
public class AuthorityFilter implements Filter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{

		List<String> errorMessages = new ArrayList<String>();

		HttpSession session = ((HttpServletRequest)req).getSession();
		User user = (User) session.getAttribute("loginUser");

        if (session.getAttribute("loginUser") == null) {
        	errorMessages.add("ユーザー情報が確認できません。再ログインしてください。");
	     	 session.setAttribute("errorMessages", errorMessages);
			((HttpServletResponse)res).sendRedirect("./login");
		return;
        } else if (session.getAttribute("loginUser") != null) {
        	String departmentId = String.valueOf(user.getDepartmentId());

	        if (!(departmentId.equals("1"))) {
	        	errorMessages.add("アクセス権限がありません");
		     	 session.setAttribute("errorMessages", errorMessages);
				((HttpServletResponse)res).sendRedirect("./");
				return;
				}
			chain.doFilter(req, res);
        }
	}
	@Override
	public void init(FilterConfig config) throws ServletException{
	}
	@Override
	public void destroy(){
	}
}