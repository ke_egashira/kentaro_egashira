package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{

		List<String> errorMessages = new ArrayList<String>();
		String ServletName = ((HttpServletRequest)req).getServletPath();
		HttpSession session = ((HttpServletRequest)req).getSession();

		System.out.println((!ServletName.matches(".*css.*") && !ServletName.equals("/login")) + ServletName);

		if (!ServletName.matches(".*css.*") && !ServletName.matches(".*login.*")) {

			if (session.getAttribute("loginUser") == null) {
				 errorMessages.add("ユーザー情報が確認できません。再ログインしてください。");
		     	 session.setAttribute("errorMessages", errorMessages);
				((HttpServletResponse)res).sendRedirect("./login");
			return;
			} else {
				User user = (User) session.getAttribute("loginUser");
		        String isStopped = String.valueOf(user.getIsStopped());
		        if (isStopped.equals("1")) {
		        	errorMessages.add("アカウントまたはパスワードが間違っています。");
			     	session.setAttribute("errorMessages", errorMessages);
			     	session.setAttribute("account", user.getAccount());
					((HttpServletResponse)res).sendRedirect("./login");
					return;
		        }
			}
		}
		chain.doFilter(req, res);
	}

	@Override
	public void init(FilterConfig config) throws ServletException{
	}
	@Override
	public void destroy(){
	}
}