package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	 @Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws IOException, ServletException {

	        request.getRequestDispatcher("message.jsp").forward(request, response);
	    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
    	 List<String> errorMessages = new ArrayList<String>();

         Message message = getMessage(request);
         if (!isValid(message, errorMessages)) {
             String title = request.getParameter("title");
             String category = request.getParameter("category");
             String text = request.getParameter("text");

             request.setAttribute("title", title);
             request.setAttribute("category", category);
             request.setAttribute("text", text);
             request.setAttribute("errorMessages", errorMessages);

             request.getRequestDispatcher("/message.jsp").forward(request, response);
             return;
         }
         HttpSession session = request.getSession();

         User user = (User) session.getAttribute("loginUser");
         message.setUserId(user.getId());

         new MessageService().insert(message);
         response.sendRedirect("./");
     }

    private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

        Message message = new Message();
        message.setTitle(request.getParameter("title"));
        message.setCategory(request.getParameter("category"));
        message.setText(request.getParameter("text"));
        return message;
    }

    private boolean isValid(Message message, List<String> errorMessages) {

    	String title = message.getTitle();
        String category = message.getCategory();
        String text = message.getText();

        if (StringUtils.isEmpty(title)) {
            errorMessages.add("タイトルを入力してください");
        } else if (30 < title.length()) {
        	errorMessages.add("タイトルは30文字以内で入力してください");
        } else if (title.matches("^( |　)+$")) {
        	errorMessages.add("タイトルを入力してください");
        }

        if (StringUtils.isEmpty(category)) {
            errorMessages.add("カテゴリを入力してください");
        } else if (10 < category.length()) {
            errorMessages.add("カテゴリは10文字以下で入力してください");
        } else if (category.matches("^( |　)+$")) {
        	errorMessages.add("カテゴリを入力してください");
        }

        if (StringUtils.isEmpty(text)) {
            errorMessages.add("本文を入力してください");
        } else if (1000 < text.length()) {
        	errorMessages.add("本文は1000文字以下で入力してください");
        } else if ((text.matches("^( |　)+$"))) {
        	errorMessages.add("本文を入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}