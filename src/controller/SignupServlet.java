package  controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        List<Department> departments = new DepartmentService().select();
        List<Branch> branches = new BranchService().select();

        request.setAttribute("departments", departments);
        request.setAttribute("branches", branches);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        List<String> errorMessages = new ArrayList<String>();
        List<Department> departments = new DepartmentService().select();
        List<Branch> branches = new BranchService().select();

        request.setAttribute("departments", departments);
        request.setAttribute("branches", branches);

        User user = getUser(request);
        if (!isValid(user, errorMessages)) {

            String account = request.getParameter("account");
            String name = request.getParameter("name");
            String branchId = String.valueOf(request.getParameter("branchId"));
            String departmentId = String.valueOf(request.getParameter("departmentId"));

            request.setAttribute("account", account);
            request.setAttribute("name", name);
            request.setAttribute("branchId", branchId);
            request.setAttribute("departmentId", departmentId);

            request.setAttribute("errorMessages", errorMessages);

            request.getRequestDispatcher("signup.jsp").forward(request, response);
            return;
        }
        new UserService().insert(user);
        response.sendRedirect("./manegement");
    }

    private User getUser(HttpServletRequest request) throws IOException, ServletException {

        User user = new User();
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setPasswordConfirm(request.getParameter("passwordConfirm"));
        user.setName(request.getParameter("name"));
        user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
        return user;
    }

    private boolean isValid(User user, List<String> errorMessages) {

    	String name = user.getName();
        String account = user.getAccount();
        String password = user.getPassword();
        String passwordConfirm = user.getPasswordConfirm();
        int branchId = user.getBranchId();
        int departmentId = user.getDepartmentId();

               User checkAccount = new UserService().selectUser(account);
        if (checkAccount != null) {
        	errorMessages.add("アカウント名が重複しています");
        }

        if (StringUtils.isEmpty(name)) {
            errorMessages.add("名前を入力してください");
        } else if (20 < name.length()) {
        	errorMessages.add("名前は20文字以下で入力してください");
        }

        if (StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if (20 < account.length()) {
            errorMessages.add("アカウント名は20文字以下で入力してください");
        } else if (5 >= account.length()) {
        	errorMessages.add("アカウント名は6文字以上で入力してください");
        } else if (!account.matches("^[a-zA-Z0-9!#$%&()*+,.:;=?@\\[\\]^_{}-]+$")){
        	errorMessages.add("アカウント名は半角英数字以外使用できません");
        }

        if (StringUtils.isEmpty(password)) {
            errorMessages.add("パスワードを入力してください");
        } else if (20 < password.length()) {
            errorMessages.add("パスワードは20文字以下で入力してください");
        } else if (5 >= password.length()) {
            errorMessages.add("パスワードは6文字以上で入力してください");
        } else if (!password.matches("^[a-zA-Z0-9!#$%&()*+,.:;=?@\\[\\]^_{}-]+$")){
        	errorMessages.add("パスワードは半角英数記号以外使用できません");
        }

        if (StringUtils.isEmpty(passwordConfirm)) {
            errorMessages.add("確認用パスワードを入力してください");
        } else if (! (passwordConfirm.equals(password))) {
            errorMessages.add("パスワードに相違があります");
        }

        if (branchId == 1 && departmentId >= 3) {
        	errorMessages.add("本社の場合、総務人事部または情報管理部以外の選択はできません");
        } else if (branchId != 1 && departmentId <= 2) {
        	errorMessages.add("支社の場合、総務人事部および情報管理部の選択はできません");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}