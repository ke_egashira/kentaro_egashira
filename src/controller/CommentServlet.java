package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
    	 List<String> errorMessages = new ArrayList<String>();
    	 HttpSession session = request.getSession();

         Comment comment = getComment(request);
         if (!isValid(comment, errorMessages)) {
             session.setAttribute("errorMessages", errorMessages);
             response.sendRedirect("./");
             return;
         }

         User user = (User) session.getAttribute("loginUser");
         comment.setUserId(user.getId());

         new CommentService().insert(comment);
         response.sendRedirect("./");
     }

    private Comment getComment(HttpServletRequest request)
    		throws IOException, ServletException {

        Comment comment = new Comment();
        comment.setText(request.getParameter("text"));
        comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));

        return comment;
    }
    private boolean isValid(Comment comment, List<String> errorMessages) {

    	String text = comment.getText();

        if (StringUtils.isEmpty(text)) {
            errorMessages.add("コメントを入力してください");
        } else if (500 < text.length()) {
        	errorMessages.add("コメントは500文字以内で入力してください");
        } else if (text.matches("^( |　)+$")) {
        	errorMessages.add("コメントを入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}