package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBranchDepartment;
import service.UserService;

@WebServlet(urlPatterns = { "/manegement" })
public class ManegementServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        List<UserBranchDepartment> usersManegement = new UserService().select();

        request.setAttribute("usersManegement", usersManegement);
        request.getRequestDispatcher("manegement.jsp").forward(request, response);
    }
}