package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {

@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {

	User user = new User();
	user.setIsStopped(Integer.parseInt(request.getParameter("isStop")));
	user.setId(Integer.parseInt(request.getParameter("userId")));

	new UserService().stateUser(user);
	response.sendRedirect("./manegement");
   }
}