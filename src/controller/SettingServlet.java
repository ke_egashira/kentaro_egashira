package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        List<Department> departments = new DepartmentService().select();
        List<Branch> branches = new BranchService().select();
        List<String> errorMessages = new ArrayList<String>();
        HttpSession session = request.getSession();

        Pattern pattern = Pattern.compile("^[0-9]*$");
        Matcher matcher = pattern.matcher(request.getParameter("userId"));

        if (!matcher.find()) {
     	   errorMessages.add("不正なパラメータが入力されました");
     	   session.setAttribute("errorMessages", errorMessages);
     	   response.sendRedirect("./manegement");
     	   return;
        }

        if (StringUtils.isEmpty(request.getParameter("userId"))) {
     	   errorMessages.add("不正なパラメータが入力されました");
     	   session.setAttribute("errorMessages", errorMessages);
     	   response.sendRedirect("./manegement");
     	  return;
        }

    	String userId = request.getParameter("userId");
    	User user = new UserService().settingSelect(Integer.parseInt(userId));

    	if (user == null) {
    		errorMessages.add("不正なパラメータが入力されました");
      	   session.setAttribute("errorMessages", errorMessages);
      	   response.sendRedirect("./manegement");
      	  return;
    	}

        session.setAttribute("departments", departments);
        session.setAttribute("branches", branches);

        request.setAttribute("user", user);
        request.getRequestDispatcher("setting.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        List<String> errorMessages = new ArrayList<String>();

        User user = getUser(request);
        int userId = Integer.parseInt(request.getParameter("userId"));
        user.setId(userId);

        if (!isValid(user, errorMessages)) {
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("user", user);
            request.getRequestDispatcher("setting.jsp").forward(request, response);
            return;
        }
        new UserService().update(user);
        response.sendRedirect("./manegement");
    }

    private User getUser(HttpServletRequest request) throws IOException, ServletException {

        User user = new User();
        user.setId(Integer.parseInt(request.getParameter("userId")));
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setPasswordConfirm(request.getParameter("passwordConfirm"));
        user.setName(request.getParameter("name"));
        user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
        return user;
    }

    private boolean isValid(User user, List<String> errorMessages) {

    	String name = user.getName();
        String account = user.getAccount();
        String password = user.getPassword();
        String passwordConfirm = user.getPasswordConfirm();
        int userId = user.getId();
        int branchId = user.getBranchId();
        int departmentId = user.getDepartmentId();

        User checkAccount = new UserService().selectUser(account);
        if (checkAccount != null) {
        	if (checkAccount.getId() != userId) {
        		errorMessages.add("アカウント名が重複しています");
        	}
        }

        if (StringUtils.isEmpty(name)) {
            errorMessages.add("名前を入力してください");
        } else if (20 < name.length()) {
        	errorMessages.add("名前は20文字以下で入力してください");
        }

        if (StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if (20 < account.length()) {
            errorMessages.add("アカウント名は20文字以下で入力してください");
        } else if (5 >= account.length()) {
        	errorMessages.add("アカウント名は6文字以上で入力してください");
        } else if (!account.matches("^[a-zA-Z0-9!#$%&()*+,.:;=?@\\[\\]^_{}-]+$")){
        	errorMessages.add("アカウント名は半角英数字以外使用できません");
        }

        if (20 < password.length()) {
            errorMessages.add("パスワードは20文字以下で入力してください");
        } else if (5 >= password.length() && 1 <= password.length()) {
            errorMessages.add("パスワードは6文字以上で入力してください");
        } else if (!(password.matches("^[a-zA-Z0-9!#$%&()*+,.:;=?@\\[\\]^_{}-]+$")) && 1 <= password.length()){
        	errorMessages.add("パスワードは半角英数記号以外使用できません");
        }

        if (! (passwordConfirm.equals(password))) {
            errorMessages.add("パスワードに相違があります");
        }

        if (branchId == 1 && departmentId >= 3) {
        	errorMessages.add("本社の場合、総務人事部または情報管理部以外の選択はできません");
        } else if (branchId != 1 && departmentId <= 2) {
        	errorMessages.add("支社の場合、総務人事部および情報管理部の選択はできません");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}