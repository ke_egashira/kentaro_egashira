package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String startDate, String endDate, String category) {

        Connection connection = null;
        try {
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	Date date =new Date();
        	String setStartDate = null;

        	if (!StringUtils.isEmpty(startDate)) {
        		setStartDate = startDate + " 00:00:00";
        	} else {
        		setStartDate = "2020-01-01 00:00:00";
        	}

        	String setEndDate = null;
        	if (!StringUtils.isEmpty(endDate)) {
        		setEndDate = endDate + " 23:59:59";
        	} else {
        		setEndDate = sdf.format(date);
        	}

        	String setCategory = null;
        	if (!StringUtils.isEmpty(category)) {
        		setCategory = "%" +category + "%";
        	} else {
        		setCategory = "%%";
        	}

            connection = getConnection();
            List<UserMessage> messages = new UserMessageDao().getMessage(connection, setStartDate, setEndDate, setCategory);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(Message message) {

	    Connection connection = null;
	    try {
	        connection = getConnection();
	        new MessageDao().delete(connection, message);
	        commit(connection);
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}